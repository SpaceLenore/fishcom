import shutil
import chessdotcom
import datetime
import json
import chess
import chess.engine
import chess.pgn
import chess.polyglot
import io
import threading
from copy import deepcopy
import os
from chessinterface import ChessInterface

clear = lambda: os.system("clear")

justify_score = lambda x: (x["score"].white().score(mate_score=1000)+500)/1000

cfg = {
    "username":"MagnusCarlsen",
    "platform": "chess.com",
    "threads":8,
    "padding":40,
    "pieces":{
        "P": "♙",
        "N": "♘",
        "B": "♗",
        "R": "♖",
        "Q": "♕",
        "K": "♔",
        "p": "♟",
        "n": "♞",
        "b": "♝",
        "r": "♜",
        "q": "♛",
        "k": "♚",
    },
    "pieces_read":{
        "p": "Black's Pawn",
        "n": "Black's Knight",
        "b": "Black's Bishop",
        "r": "Black's Rook",
        "q": "Black's Queen",
        "k": "Black's King",
        "P": "White's Pawn",
        "N": "White's Knight",
        "B": "White's Bishop",
        "R": "White's Rook",
        "Q": "White's Queen",
        "K": "White's King",
    }
}
try:
    with open("config.json") as inf:
        cfg = json.load(inf)
except:
    with open("config.json", "w") as outf:
        json.dump(cfg, outf, indent=4)
    
UN = cfg["username"]

PLATFORM = cfg["platform"]

STONKFISH = shutil.which("stockfish") #TODO: Detect if no stockfish exists and exit with error

PADDING = cfg["padding"]

THREADS = cfg["threads"]

PIECES = cfg["pieces"]

PIECES_READ = cfg["pieces_read"]

chessPlatform = ChessInterface(PLATFORM, UN)


def get_all_games() -> list[chess.pgn.Game]:
    gameout = []
    games = chessPlatform.getLastMonthGames()
    for game in games.split("\n\n\n"):
        gameout.append(chess.pgn.read_game(io.StringIO(game)))
    return gameout


def game_picker(presel=None) -> chess.pgn.Game:
    games = get_all_games()
    try:
        if presel is not None:
            return games[presel]
        else:
            header = f"{'nr'.center(4)}: {'White'.center(PADDING)}    {'Black'.center(PADDING)}{'Result'.center(10)}{'Date'.center(10)}"
            print(header)
            print("-" * len(header))
            for i, game in enumerate(games):
                print(f"{str(i).center(4)}: {game.headers['White'].center(PADDING)} vs {game.headers['Black'].center(PADDING)}{game.headers['Result'].center(10)}{game.headers['Date'].center(10)}")
            return games[int(input("Pick a game: "))]
    except:
        print("Ok I'll just not then")
        quit()

    
def analyse_game(game: chess.pgn.Game, output: list, loc=STONKFISH, limit=chess.engine.Limit(depth=14), threads=THREADS) -> chess.engine.InfoDict:
    opts = {
        "Threads": threads,
    }
    engine = chess.engine.SimpleEngine.popen_uci(loc)
    engine.configure(opts)
    base_board = game.board()
    for i, move in enumerate(game.mainline_moves()):
        base_board.push(move)
        output[i] = engine.analyse(base_board, limit)
    engine.quit()

def captured(boardstr: str) -> tuple[str, str]:
    wcount = {
        "♟":8,
        "♞":2,
        "♝":2,
        "♜":2,
        "♛":1,
        "♚":1
    }
    bcount = {
        "♙":8,
        "♘":2,
        "♗":2,
        "♖":2,
        "♕":1,
        "♔":1
    }
    points = {
        "♙":1,
        "♘":3,
        "♗":3,
        "♖":4,
        "♕":7,
        "♔":1,
        "♟":1,
        "♞":3,
        "♝":3,
        "♜":4,
        "♛":7,
        "♚":100,
    }
    wstr = ""
    bstr = ""
    for piece in wcount:
        wstr += piece * (wcount[piece] - boardstr.count(piece))

    for piece in bcount:
        bstr += piece * (bcount[piece] - boardstr.count(piece))

    wscore = sum([points[x] for x in wstr])
    bscore = sum([points[x] for x in bstr])

    wstr += " " + ("+" +str(wscore - bscore) if wscore > bscore else " ")
    bstr += " " + ("+" +str(bscore - wscore) if bscore > wscore else " ") 
    return (wstr, bstr)


def gen_state_string(game: chess.pgn.GameNode) -> str:
    #Create board
    board_state = pieceify(str(game.board()))
    board_strings =  [ "╔═══╤" + "═" * 17 + "╦"]
    board_strings += [f"║ {8-y} │ {x} ║" for y, x in enumerate(str(board_state).split("\n"))]
    board_strings += [ "╟───┼" + "─" * 17 + "╢"]
    board_strings += [ "║   │ " + " ".join([chr(x) for x in range(65, 73)]) +" ║"]
    board_strings += [ "╚═══╧" + "═" * 17 + "╩"]

    #Create board names
    black_player = str(game.game().headers['Black'])
    white_player = str(game.game().headers['White'])
    white_captures, black_captures = captured(board_state)
    name_pad = max(len(black_player), len(white_player))+10
    board_strings[0] = board_strings[0] + "═" * name_pad + "╗"
    if game.board().turn == chess.WHITE:
        white_player = f">> {white_player} <<"
    else:
        black_player = f">> {black_player} <<"
    for i in range(1, len(board_strings)-1):
        if i == 2:
            board_strings[i] = board_strings[i] + black_player.center(name_pad) + "║"
            continue
        elif i == 3:
            board_strings[i] = board_strings[i] + white_captures.center(name_pad) + "║"
            continue
        elif i == len(board_strings)-4:
            board_strings[i] = board_strings[i] + black_captures.center(name_pad) + "║"
            continue
        elif i == len(board_strings)-3:
            board_strings[i] = board_strings[i] + white_player.center(name_pad) + "║"
            continue
        board_strings[i] = board_strings[i] + " " * name_pad + "║"
    board_strings[-1] = board_strings[-1] + "═" * name_pad + "╝"
    return "\n".join(board_strings)

def get_movestr(sans: list[str], move: int, maxlen) -> str:
    move = move+(maxlen//10)-3
    outstr = "".join([x.center(5) for x in sans[(move-1 - maxlen//10 if move-1 - maxlen//10 > 0 else 0):(move + maxlen//10 if move + maxlen//10 < len(sans) else len(sans))]])
    outstr = outstr.rjust(maxlen, " ") if move < len(sans)//2 else outstr.ljust(maxlen, " ")
    return outstr


def get_adv_bar(analysis: chess.engine.InfoDict, maxlen: int) -> str:
    adv_bar = ""
    score = justify_score(analysis)
    adv_bar += ("█" * int(score*maxlen)).ljust(maxlen, "░")
    return adv_bar

def get_matein(analysis: chess.engine.InfoDict) -> str:
    outstr = "No mate"
    if type(analysis["score"].white()) != type(chess.engine.Cp) and analysis["score"].white().is_mate():
        if analysis["score"].white().mate() > 0:
            outstr = f"White mates in {analysis['score'].white().mate()}"
        elif analysis["score"].white().mate() < 0:
            outstr = f"Black mates in {-analysis['score'].white().mate()}"
        elif type(analysis["score"].white().mate()) == type(chess.engine.MateGiven):
            outstr = "Black mated"
        elif analysis["score"].white().mate() == 0:
            outstr = "White mated"
    return outstr

def get_moverate(game: chess.pgn.GameNode, analysis: list[chess.engine.InfoDict]) -> str:
    moverate = ""
    if len(analysis) > 1 and game.parent:
        prevscore = justify_score(analysis[0])
        curscore = justify_score(analysis[1])
        curscore = 0
        bestmove = game.parent.board().san(analysis[0]["pv"][0])
        curmove = game.parent.board().san(game.move)
        if  bestmove == curmove:
            moverate = f"{curmove} is the best move!"
        elif curscore > prevscore:
            moverate = f"{curmove} move is a strong move, but {bestmove} is better"
        elif curscore <= prevscore:
            moverate = f"{curmove} move is weak, {bestmove} is better"
        else:
            moverate = "no data on this move"
    return moverate

def gen_analysis(game: chess.pgn.GameNode, analysis: chess.engine.InfoDict, opening_tree: dict[str:dict], board_state: str, move: int) -> str:
    move = move -1
    all_sans = get_san(game.game().next())
    read_moves = readable_moves(all_sans)
    opening_names = get_opening(all_sans, opening_tree)
    board_state = board_state.split("\n")
    board_width = len(board_state[0])
    cur_opening = opening_names[move] if opening_names[move] else ""
    moverate = get_moverate(game, analysis[move-1:])
    adv_bar = get_adv_bar(analysis[move], board_width-4)
    adv_stat = str(justify_score(analysis[move]))
    wdl = analysis[move]["score"].white().wdl()
    matein = get_matein(analysis[move])
    movestr = get_movestr(all_sans, move, board_width-4)
    board_state[-1] = "╠" + board_state[-1][1:-1] + "╣"
    #opening
    if cur_opening:
        board_state += ["║" + " "*(board_width-2) + "║"]
        board_state += ["║" + "Opening:".center(board_width-2)+ "║"]
        board_state += ["║" + cur_opening.center(board_width-2) + "║"]
        board_state += ["║" + " "*(board_width-2) + "║"]
        board_state += ["╟" + "─" * (board_width-2) + "╢"]
    #move
    board_state += ["║" + " "*(board_width-2) + "║"]
    board_state += ["║" + read_moves[move].center(board_width-2) + "║"]
    board_state += ["║" + movestr.center(board_width-2) + "║"]
    board_state += ["║" + " "*(board_width-2) + "║"]
    board_state += ["║" + (moverate.center(board_width-2) if moverate else " "*(board_width-2)) + "║"]
    board_state += ["║" + " "*(board_width-2) + "║"]
    board_state += ["╟" + "─" * (board_width-2) + "╢"]
    #advantage
    board_state += ["║" + " "*(board_width-2) + "║"]
    board_state += ["║" + " White" + adv_stat.center(board_width-14) + "Black " + "║"]
    board_state += ["║ " + adv_bar.ljust(board_width-4) + " ║"]
    board_state += ["║ " + str(round(wdl.winning_chance()*100, 5)).ljust(5) + str(round(wdl.drawing_chance()*100, 4)).center(board_width-14) + str(round(wdl.losing_chance()*100, 4)).rjust(5) +" ║"]
    board_state += ["║" + matein.center(board_width-2) + "║"]
    board_state += ["║" + " "*(board_width-2) + "║"]
    board_state += ["╚" + "═"*(board_width-2) + "╝"]
    
    return "\n".join(board_state)

def readable_move(move, white=True) -> str:
    outmove = ""
    if len(move) == 2:
        outmove = f"{PIECES_READ['P'] if white else PIECES_READ['p']} to {move}"
    elif move == "O-O":
        outmove = f"{'White' if white else 'Black'} short castles"
    elif move == "O-O-O":
        outmove = f"{'White' if white else 'Black'} long castles"
    elif "x" not in move and "=" not in move:
        outmove = f"{PIECES_READ[move[0]]} to {move[1:]}"
    elif "x" in move and "=" not in move:
        try:
            outmove = f"{PIECES_READ[move[0]]} takes on {move[2:]}"
        except KeyError:
            outmove = f'{"White" if white else "Black"}\'s {move[0]} pawn takes on {move[2:]}'
    elif "x" not in move and "=" in move:
        outmove = f"{PIECES_READ[move[0]]} promotes to {PIECES_READ[move[-1]]} on {move[1:3]}"
    else:
        outmove = move
    if "+" in move:
        outmove = outmove[:-1] + ", check"
    elif "#" in move:
        outmove = outmove[:-1] + ", checkmate"
    return outmove

def readable_moves(sans, white = True) -> str:
    outmoves = []
    for move in sans:
        outmoves.append(readable_move(move, white))
        white = not white
    return outmoves

def pieceify(string: str) -> str:
    for key,value in PIECES.items():
        string = string.replace(key, value)
    return string

def get_opening(sans: list[str], openings: dict[str:dict]) -> list:
    names = []
    tmp = deepcopy(openings)
    for move in sans:
        if move in tmp and "NAME" in tmp[move]:
            names.append(tmp[move]["NAME"])
        else:
            names.append(None)
        if move in tmp:
            tmp = tmp[move]
    return names


def get_san(game: chess.pgn.ChildNode) -> list[str]:
    if game:
        return [game.san()] + get_san(game.next())
    return []

def interactive():
    with open("opening-tree.json", "r") as f:
        opening_tree = json.load(f)
    game = game_picker()
    clear()
    mainline_moves = [x for x in game.mainline_moves()]
    analysis = ["Analysing..."] * len(mainline_moves)
    analysis_thread = threading.Thread(target=analyse_game, args=(deepcopy(game), analysis))
    analysis_thread.start()
    done = False
    move = 0
    san_moves = get_san(game.next())
    white_moves = f"White moves: {' '.join([x.ljust(5) for x in san_moves[::2]])}"
    black_moves = f"Black moves: {' '.join([x.ljust(5) for x in san_moves[1::2]])}"
    winstr = f"{'White' if game.headers['Result'][0] == '1' else 'Black'} wins by {'checkmate' if '#' in san_moves[-1] else 'time'}" if "." not in game.headers['Result'] else "Draw"
    print()
    print(f"{game.headers['White'].center(PADDING)} vs {game.headers['Black'].center(PADDING)}".center(max(len(black_moves), len(white_moves))))
    print(game.headers['Date'].center(max(len(black_moves), len(white_moves))))
    print(winstr.center(max(len(black_moves), len(white_moves))))
    print()
    print(white_moves)
    print(black_moves)
    input()
    clear()
    while not done:
        board_state = gen_state_string(game)
        if move > 0:
            display = gen_analysis(game, analysis, opening_tree, board_state, move)
        else:
            display = board_state
        print(display)

        if analysis.count('Analysing...') == len(analysis):
            print(f"\n\nAnalysis status: {len(analysis) - analysis.count('Analysing...')}/{len(analysis)}")
        print(f"Move: {move}/{len(mainline_moves)}")
        print("[N]ext move, [P]revious move, [Q]uit")
        inp = input("> ").lower()
        if inp == "n":
            if game.next():
                if analysis[move] == 'Analysing...':
                    print("stil analysing")
                else:
                    move += 1
                    game = game.next()
            elif move == len(mainline_moves):
                move = 0
                game = game.game()
        elif inp == "p":
            if game.parent:
                move -= 1
                game = game.parent
            elif move == 0:
                move = len(analysis) - analysis.count('Analysing...')
                game = game.game()
                for _ in range(move):
                    game = game.next()
        elif inp == "b":
            print(analysis[move])
        elif inp == "r":
            game = game.game()
            move = 0
        elif inp == "q":
            done = True
    clear()
    print("Shutting down...")
    analysis_thread.join()
        


def main():
    #asyncio.set_event_loop_policy(chess.engine.EventLoopPolicy())
    interactive()

if __name__ == "__main__":
    main()