# fishcom
Its justs a lil' chess guy. You wouldnt hurt a lil' chess guy would ya?

## install
```
git clone git@gitlab.com:Alice_Alisceon/fishcom.git
python -m venv env
source env/bin/activate
pip install -r requirements.txt
python fishcom.py
```
## Thanks
kudos to [lichess](https://github.com/lichess-org/chess-openings.git) for the database I made the openings tree from