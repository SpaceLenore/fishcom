import json

#get base data from https://github.com/lichess-org/chess-openings.git

lines = []
for fn in "abcde":
    with open(f"chess-openings/{fn}.tsv", "r") as f:
        lines += f.readlines()

pgn_to_name = {}
for line in lines:
    line = line.strip().split("\t")
    pgn_to_name[line[2]] = line[1]

pgns = sorted(pgn_to_name.keys())

def build_move_tree(move_list, name, move_tree={}):
    if not move_list:
        return {"NAME":name}
    if move_list[0] not in move_tree:
        move_tree[move_list[0]] = {}
    move_tree[move_list[0]] = build_move_tree(move_list[1:], name, move_tree[move_list[0]])
    return move_tree

move_tree = {}
for pgn in pgns:
    move_tree = build_move_tree([x for x in pgn.split(" ") if "." not in x], pgn_to_name[pgn], move_tree)

with open("opening-tree.json", "w") as f:
    json.dump(move_tree, f, indent=4)