import datetime
import time
import chessdotcom
import lichess.api
from lichess.format import SINGLE_PGN

class ChessInterface:
    def __init__(self, platform, username):
        self.source = platform
        self.username = username

    def getLastMonthGames(self,):
        if self.source == "chess.com":
            return self._chessdotcomLatestGames()
        elif self.source == "lichess.org":
            return self._lichessLatestGames()
        else:
            raise Exception("Error! chess source is invalid")

    def _lichessLatestGames(self):
        month = datetime.datetime.now() - datetime.timedelta(days=30) # last 30 days rolling is ~ 1 month
        dt = int(time.mktime(month.timetuple()))
        return lichess.api.user_games(self.username, since=dt, format=SINGLE_PGN).rstrip()
        
    def _chessdotcomLatestGames(self):
        dt = datetime.datetime.now()
        return chessdotcom.get_player_games_by_month_pgn(self.username, datetime_obj=dt).json["pgn"]["pgn"]
